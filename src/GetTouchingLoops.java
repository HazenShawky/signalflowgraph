
import java.util.*;

/**
 * This class is responsible for computing all relations between non-touching loops and finds all combinations of loops
 *
 *
 */
public class GetTouchingLoops {
    private List<Loop> loops;// loops list
    public Map<Integer, Double> hashMap = new HashMap<>();// maps the mask(which represents the loops as bits) to their gain.

    /**
     * The constructor which takes the loops and calls preCompute
     *
     * @param loops
     */
    public GetTouchingLoops(List<Loop> loops){
        this.loops = loops;
        for(int i = 0; i < loops.size(); ++i)
            hashMap.put(1<<i, gainOfOneLoop(loops.get(i)));
        preCompute(0, 0);
    }

    /**
     * calculates the gain of a single loop.
     * @param loop
     * @return the gain of this loop
     */
    private double gainOfOneLoop(Loop loop){
        double ans = 1;
        for(Edge edge: loop.loop){
            ans*=edge.weight;
        }
        return ans;
    }

    /**
     * recursive method to find all combination of loops.
     * storing the loops in the mask to save the space.
     * (i.e: if we have 32 loop and we have loop 1, 2, 5 in this set the mask will be 00000000000000000000000000010011)
     *
     * (Note: the possibility that mask overflows is impossible as the complexity of the method below is
     * O(2^n) which means we can solve to loops up to only 20 which is less than 32)
     *
     *
     * @param idx
     * @param mask
     */
    private void preCompute(int idx, int mask){
        if(idx==loops.size()){
            if(mask==0 || (mask&(mask-1))==0)
                return;
            hashMap.put(mask, (non_touching(mask) ? calculateGain(mask) : 0));
            return;
        }
        preCompute(idx+1, mask);
        preCompute(idx+1, (mask|(1<<idx)));
    }

    /**
     * getting set of the loops.
     * @param mask
     * @return true if this set of loops is non-touching.
     */
    private boolean non_touching(int mask){
        Set<Integer> hashSet = new HashSet<>();
        for(int i = 0; i < loops.size(); ++i){
            if((mask&(1<<i))!=0){
                for(Edge edge : loops.get(i).loop){
                    if(hashSet.contains(edge.to))
                        return false;
                    hashSet.add(edge.to);
                }
            }
        }
        return true;
    }

    /**
     * calculates the gain of a set of loops.
     * @param mask
     * @return the gain of the set of loops.
     */
    private double calculateGain(int mask){
        double ans = 1;
        for(int i = 0; i < loops.size(); ++i){
            if((mask&(1<<i))!=0){
                ans*=hashMap.get(1<<i);
            }
        }
        return ans;
    }
}
