import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class contains the graph implementation using adjacency list and methods to get paths and loops
 */
public class MyGraph {
    public List<Edge>[] adjacentList;//storing the graph in adjacency list.
    public  List<Path> paths;//list of all paths from source to sink node.
    public List<Loop> loops; //list of all loops between source and sink node.
    public int start, end;// start and sink node.

    /**
     * The constructor of the graph which takes source node, sink node and number of nodes.
     * @param s
     * @param e
     * @param n
     */
    public MyGraph(int s, int e, int n){
        this.adjacentList = new List[n+1];
        for(int i = 1; i <= n; ++i)
            adjacentList[i] = new ArrayList<>();
        this.start = s;
        this.end = e;
        paths = new ArrayList<>();
        loops = new ArrayList<>();
    }

    /**
     * To add new edge to the graph.
     * @param from
     * @param to
     * @param weight
     */
    public void addEdge(int from, int to, double weight){
        adjacentList[from].add(new Edge(from, to, weight));
    }

    /**
     * calls the dfs method to get all paths and loops between source and sink node.
     * calls removeDuplicates to remove the duplicated loops after dfs.
     */
    public void preCompute(){
        boolean[] visited = new boolean[adjacentList.length];
        dfs(start, new Path(), visited);
        removeDuplicates();
    }

    /**
     * A recursive method to find all paths and loops between source and sink node.
     * @param v
     * @param curPath
     * @param visited
     */
    private void dfs(int v, Path curPath, boolean[] visited){
        if(v==end){
            paths.add(new Path(curPath));
            return;
        }
        visited[v] = true;
        for(Edge u : adjacentList[v]){
            if(visited[u.to]){
                loops.add(new Loop(curPath, u));
            }else{
                curPath.addVertex(u);
                dfs(u.to, curPath, visited);
                curPath.removeVertex();
            }
        }
        visited[v] = false;
    }

    /**
     * Deletes duplicated loops generated from the dfs
     */
    private void removeDuplicates(){
        for(int i = 0; i < loops.size(); ++i){
            Set<Edge> st1 = new HashSet<>();
            for(int j = 0; j < loops.get(i).loop.size(); ++j)
                st1.add(loops.get(i).loop.get(j));
            for(int j = i+1; j < loops.size(); ++j){
                Set<Edge> st2 = new HashSet<>();
                for(Edge edge: loops.get(j).loop)
                    st2.add(edge);
                if(st1.size()==st2.size()){
                    st2.removeAll(st1);
                    if(st2.isEmpty()){
                        loops.remove(j--);
                    }
                }
            }
        }
    }
}