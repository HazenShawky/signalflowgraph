import com.brunomnsilva.smartgraph.graph.Digraph;
import com.brunomnsilva.smartgraph.graph.DigraphEdgeList;
import com.brunomnsilva.smartgraph.graphview.SmartCircularSortedPlacementStrategy;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;
import com.brunomnsilva.smartgraph.graphview.SmartPlacementStrategy;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is the controller of the GUI.
 */

public class Controller {
    private MyGraph myGraph;
    @FXML
    private Button selectButton, addEdgeButton, solveButton;
    @FXML
    private TextField from, to, weight, source, sink, numOfNodes, answer;
    @FXML
    private ListView<String> loops, paths;
    @FXML
    public void select(){
        try{
            myGraph = new MyGraph(Integer.parseInt(source.getText()), Integer.parseInt(sink.getText()), Integer.parseInt(numOfNodes.getText()));
        }catch(Exception e){
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText("Please Enter a valid number of node, source and sink node");
            errorAlert.showAndWait();
            return;
        }
        from.setDisable(false);
        to.setDisable(false);
        weight.setDisable(false);
        addEdgeButton.setDisable(false);
        selectButton.setVisible(false);
        source.setDisable(true);
        sink.setDisable(true);
        numOfNodes.setDisable(true);
        drawGraph();
    }
    @FXML
    public void addEdge(){
        try{
            myGraph.addEdge(Integer.parseInt(from.getText()), Integer.parseInt(to.getText()), Double.parseDouble(weight.getText()));
        }catch(Exception e){
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText("Please Enter a valid edge");
            errorAlert.showAndWait();
            return;
        }
        drawGraph();
    }
    @FXML
    public void solve(){
        if(myGraph.adjacentList[myGraph.end].size()!=0){
            myGraph.addEdge(myGraph.end, 0, 1);
            myGraph.end = 0;
            myGraph.adjacentList[0] = new ArrayList<>();
            sink.setText("0");
            drawGraph();
        }
        myGraph.preCompute();
        for(Loop loop : myGraph.loops){
            String str = "V"+loop.loop.get(0).from;
            for(Edge edge : loop.loop){
                str = str+", V"+edge.to;
            }
            loops.getItems().add(str);
        }
        for(Path path: myGraph.paths){
            String str = "V"+path.path.get(0).from;
            for(Edge edge : path.path){
                str = str+", V"+edge.to;
            }
            paths.getItems().add(str);
        }
        GetTouchingLoops getTouchingLoops = new GetTouchingLoops(myGraph.loops);
        double ans = 0;
        for(Path path : myGraph.paths) {
            CalculateDelta calculateDelta = new CalculateDelta(getTouchingLoops, path, myGraph.loops);
            ans+=calculateDelta.getAns();
        }
        answer.setText(String.valueOf(ans/(new CalculateDelta(getTouchingLoops, null, myGraph.loops).getAns())));
        solveButton.setDisable(true);
    }
    private void drawGraph(){
        Digraph<String, Double> graph = new DigraphEdgeList<>();
        for(int i = 0; i < myGraph.adjacentList.length; ++i){
            if(myGraph.adjacentList[i]==null)
                continue;
            graph.insertVertex("V"+i);
        }
        for(List<Edge> list : myGraph.adjacentList){
            if(list==null)
                continue;
            for(Edge edge : list){
                graph.insertEdge("V"+edge.from, "V"+edge.to, edge.weight);
            }
        }
        Main.root.getChildren().remove(Main.root.getChildren().size()-1);
        SmartPlacementStrategy strategy = new SmartCircularSortedPlacementStrategy();
        SmartGraphPanel<String, Double> graphView = new SmartGraphPanel<>(graph, strategy);
        graphView.setLayoutX(0);
        graphView.setLayoutY(160);
        graphView.setPrefSize(1000, 550);
        //graphView.setAutomaticLayout(true);
        graphView.getStylableVertex("V"+myGraph.start).setStyleClass("start");
        graphView.getStylableVertex("V"+myGraph.end).setStyleClass("end");
        Main.root.getChildren().add(graphView);
        MultiThread multiThread = new MultiThread(graphView);
        multiThread.start();
    }
    class MultiThread extends Thread {
        SmartGraphPanel<String, Double> graphView;
        public MultiThread(SmartGraphPanel<String, Double> gv){
            graphView = gv;
        }
        @Override
        public void run(){
            while(graphView.getWidth()==0||graphView.getHeight()==0){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            graphView.init();
        }
    }
}
