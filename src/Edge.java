/**
 * This class contains the implementation of the Edge.
 */
public  class Edge {
    int  to, from;
    double weight;

    /**
     * The constructor which takes the source vertex of the edge, the destination vertex and the weight of this edge
     * @param f
     * @param t
     * @param w
     */
    public Edge(int f, int t, double w){
        to = t;
        weight = w;
        from = f;
    }

    /**
     * Overrides the equal method to check if two edges are identical.
     * @param otherEdge
     * @return
     */
    @Override
    public boolean equals(Object otherEdge){
        if(otherEdge instanceof Edge) {
            return (to == ((Edge) otherEdge).to) && (from == ((Edge) otherEdge).from) && (Math.abs(weight - ((Edge) otherEdge).weight) < 1e-9);
        }
        return false;
    }
}