import java.util.ArrayList;
import java.util.List;

/**
 * This class includes the implementation of the loop
 */
public class Loop {
    List<Edge> loop;

    /**
     * The constructor takes the current path and edge which has reverse edge.
     * then exclude the edge from the path.
     *
     * @param path
     * @param v
     */
    public Loop(Path path, Edge v){
        loop = new ArrayList<>();
        boolean f = false;
        for(Edge u : path.path){
            if(u.from ==v.to){
                f = true;
            }
            if(f){
                loop.add(u);
            }
        }
        loop.add(v);
    }
}