import com.brunomnsilva.smartgraph.graph.Digraph;
import com.brunomnsilva.smartgraph.graph.DigraphEdgeList;
import com.brunomnsilva.smartgraph.graphview.SmartCircularSortedPlacementStrategy;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;
import com.brunomnsilva.smartgraph.graphview.SmartPlacementStrategy;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * The start of the GUI.
 */
public class Main extends Application {
    public static Pane root;
    @Override
    public void start(Stage primaryStage) throws Exception{
        root = FXMLLoader.load(getClass().getResource("Main.fxml"));
        Digraph<String, Double> graph =  new DigraphEdgeList<>();
        SmartPlacementStrategy strategy = new SmartCircularSortedPlacementStrategy();
        SmartGraphPanel<String, Double> graphView = new SmartGraphPanel<>(graph, strategy);
        graphView.setLayoutX(0);
        graphView.setLayoutY(160);
        graphView.setPrefSize(1000, 550);
        //graphView.setAutomaticLayout(true);
        root.getChildren().add(graphView);
        primaryStage.setTitle("Signal Flow Graph");
        primaryStage.setScene(new Scene(root, 1005, 720));
        primaryStage.setResizable(false);
        primaryStage.show();
        graphView.init();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
