import java.util.ArrayList;
import java.util.List;
/**
 * This class includes the implementation of the path
 */
public class Path{
    List<Edge> path;

    /**
     * The default constructor.
     */
    public Path(){
        path = new ArrayList<>();
    }

    /**
     * creates new path from another one(clone the path)
     * @param p
     */
    public Path(Path p){
        path = new ArrayList<>(p.path);
    }

    /**
     * add next edge to the current path.
     * @param v
     */
    public void addVertex(Edge v){
        path.add(v);
    }

    /**
     * remove the last edge in the current path.
     */
    public void removeVertex(){
        path.remove(path.size()-1);
    }
}