import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class calculates the deltas given all loops, paths and non-touching loops.
 */
public class CalculateDelta {

    GetTouchingLoops touchingLoops;
    Path path;
    List<Loop> loops;
    double ans;
    public CalculateDelta(GetTouchingLoops touchingLoops, Path path, List<Loop> allLoops){
        this.touchingLoops = touchingLoops;
        this.path = path;
        this.loops = new ArrayList<>();
        ans = 1;
        calculateAns(allLoops);
    }
    private void calculateAns(List<Loop> allLoops){
        Set<Integer> hashSet = new HashSet<>();
        double pathGain = 1;
        if(path==null) {
            loops = allLoops;
        }else {
            hashSet.add(path.path.get(0).from);
            for (Edge edge : path.path) {
                pathGain*=edge.weight;

                hashSet.add(edge.to);
            }
            for (int i = 0; i < allLoops.size(); ++i) {
                boolean f = true;
                for (Edge edge : allLoops.get(i).loop) {
                    if (hashSet.contains(edge.to)) {
                        f = false;
                        break;
                    }
                }
                if(f)
                    loops.add(allLoops.get(i));
                else
                    loops.add(null);
            }
        }
        for(int i = 0; i < loops.size(); ++i){
            if(i%2==0){
                ans-=delta(0, 0, i+1);
            }else{
                ans+=delta(0, 0, i+1);
            }
        }
        ans*=pathGain;
    }
    private double delta(int idx, int mask, int cnt){
        if(cnt<0)
            return 0;
        if(idx==loops.size()){
            if(cnt!=0)
                return 0;
            return touchingLoops.hashMap.get(mask);
        }
        if(loops.get(idx)==null)
            return delta(idx+1, mask, cnt);
        return delta(idx+1, mask, cnt)+delta(idx+1, (mask|(1<<idx)), cnt-1);
    }
    public double getAns(){
        return ans;
    }

}
